# Introduction
licenseCreator is a small C program which aims to allow you to add a ```LICENSE``` file to your project with your credentials.
# Compilation
To compile licenseCreator clone the git respository into a folder and run the ```compile.sh``` file within. NOTE: This currently only works on Linux. MacOS aswell as Windows have not been tested.
# Usage
This will be a small guide to help you use licenseCreator in your workflow.
## First setup
To first setup licenseCreator follow these simple steps:
1.  Download the git repository and compile it according to the instructions in the compilation section.
2.  Copy the executable named ```licenseCreator``` into your ```/bin/.``` folder.
3.  Now run the command ```licenseCreator init [FirstName] [LastName] [Email]``` to setup the configuration.
4.  Now add your licenses as plain text files to the directory ```~/.config/licenseCreator/licenses/.```. NOTE: The file name will be used to write a license so name your files something easy to type.
5.  Now you can run ```licenseCreator write [NameOfLicenseFile]``` to write a license in the current directory.
## After setup
If you already setup licenseCreator you can just write ```licenseCreator write [NameOfLicenseFile]``` to write a license in the current directory.
## Changing your credentials
To change your credentials in licenseCreator just run the command ```licenseCreator init [FirstName] [LastName] [Email]``` again with your new credentials.
## Adding your own license
To add your own license to licenseCreator just put your license into the ```~/.config/licenseCreator/licenses/.``` directory. There is an option to have your credentials added in the license automatically by simply adding ```%s``` aswell ```%d``` in the following order:<br>
```
%d      %s      %s      %s
|       |       |       |
year    first   last    email
        name    name
```
At this point the order of the tokens is only editible by either changing the source code directly, editing the ```~/.config/licenseCreator/config.conf``` file directly, or chaning the order in which you enter your credentials into the ```init``` command.
