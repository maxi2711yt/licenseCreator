#pragma once
#include "defines.h"

#include "../include/config.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>

void checkConfig(const char* path); // If path==NULL then path will be set to CONFIG_PATH
void readConfig(config* conf); // Reads the config file located in ~/CONFIG_PATH/CONFIG_FILE_NAME
void writeConfig(const config conf); // Writes a config struct to a file located in ~/CONFIG_PATH/CONFIG_FILE_NAME
