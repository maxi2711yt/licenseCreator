#pragma once

#define CONFIG_PATH         "/.config/licenseCreator/"
#define CONFIG_FILE_NAME    "config.conf"
#define LICENSE_PATH        "/.config/licenseCreator/licenses/"
#define CWD_PATH_SIZE       1024
#define LICENSE_NAME        "/LICENSE"

typedef struct {
    const char* first_name;
    const char* last_name;
    const char* email;
} config; // Houses all information needed to store a config.
