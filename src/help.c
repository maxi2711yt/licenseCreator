#include "../include/help.h"
#include "../include/defines.h"

void help()
{
    printf("Usage:\nlicenseCreator [parameter] [options]\n");
    printf("parameters:\n\tinit [FistName] [LastName] [Email]\tCreates the config file\n");
    printf("\twrite [license]\t\t\t\tWrites the license in the current directory.\n");
    printf("\tcustom [license] [FirstName] [LastName] [Email]\tWrites the specified license with custom credentials "
           "into the current directory.\n");
}
