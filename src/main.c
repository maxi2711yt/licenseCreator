#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <time.h>

#include <sys/stat.h>

#include "../include/config.h"
#include "../include/defines.h"
#include "../include/help.h"

static char homedir[CWD_PATH_SIZE];
static char cwd[CWD_PATH_SIZE];

void writeLicense(const config conf, const char* lname)
{

#ifdef DEBUG
        printf("Finding and reading license content.\n");
#endif
        FILE *lF;

#ifdef DEBUG
        printf("Generating path...\n");
#endif
        char lpath[CWD_PATH_SIZE];

        strcpy(lpath, homedir);
        strcat(lpath, LICENSE_PATH);
        strcat(lpath, lname);
#ifdef DEBUG
        printf("Generated \"%s\"", lpath);
        printf("Opening file...\n");
#endif

        lF = fopen(lpath, "r");

        if (lF == NULL)
        {
            fprintf(stderr, "Couldn't open license file at \"%s\", check that that license exists.\n", lpath);
            exit(1);
        }
#ifdef DEBUG
        printf("Done!\n");
        printf("Calculating file size...\n");
#endif
        size_t size;
        fseek(lF, 0, SEEK_END);
        size = ftell(lF);
        rewind(lF);
#ifdef DEBUG
        printf("Done! File is of size %ld.\n", size);
        printf("Reading file...\n");
#endif
        char *buf = malloc(size);

        fread(buf, sizeof(char), size, lF);

        fclose(lF);

#ifdef DEBUG
        printf("Done!\n");
#endif
        FILE *f;

#ifdef DEBUG
        printf("Creating path for LICENSE file...\n");
#endif
        char path[CWD_PATH_SIZE];
        strcpy(path, cwd);
        strcat(path, LICENSE_NAME);
#ifdef DEBUG
        printf("Generated path \"%s\"\n", path);
        printf("Opening file...\n");
#endif
        f = fopen(path, "w");
#ifdef DEBUG
        printf("Done!\n");
#endif
        if (f == NULL)
        {
            fprintf(stderr, "Couldn't open LICENSE file.\n");
            exit(1);
        }

#ifdef DEBUG
        printf("Getting local time...\n");
#endif
        time_t t = time(NULL);
        struct tm tm_s = *localtime(&t);

#ifdef DEBUG
        printf("Done!\n");
        printf("Printing to file...\n");
#endif
        fprintf(f, buf, tm_s.tm_year + 1900, conf.first_name, conf.last_name, conf.email);
#ifdef DEBUG
        printf("Done!\n");
#endif
        fclose(f);
}

int main(int argc, char **argv)
{
    // Getting current working dir
#ifdef DEBUG
    printf("Getting current working directory...\n");
#endif
    getcwd(cwd, sizeof(cwd));

#ifdef DEBUG
    printf("Done!\n");
#endif

#ifdef DEBUG
    printf("Getting HOME directory...\n");
#endif
    strcpy(homedir, getenv("HOME"));
    // Parsing command line arguments
    // Checking if atleast 3 parameters exist
#ifdef DEBUG
    printf("Checking if argc < 3...\n");
#endif
    if (argc < 3)
    {
        fprintf(stderr, "Insufficent number of arguments provided.\n");
        help();
        exit(1);
    }
#ifdef DEBUG
    printf("Done!\n");
#endif

    // Parsing parameter
    // Valid options:
    // init   Initilialize the config file in ~/.config/licenseCreator/config.conf
    // write   Write a license using the in the config file used credentials in the current directory
    // custom  Write a license with provided credentials in the current directory

    config conf = {0};
    const char *parameter = argv[1];
    if (strcmp(parameter, "init") == 0)
    {
        // Parsing arguments
        // licenseCreator init [FirstName] [LastName] [Email]
#ifdef DEBUG
        printf("Parameter \"init\" passed. Checking if sufficent number of arguments have been passed...\n");
#endif
        if (argc < 5)
        {
#ifdef DEBUG
            printf("Insufficent number of arguments passed!\n");
#endif
            fprintf(stderr, "Insufficent number of arguments!\n");
            help();
            exit(1);
        }

#ifdef DEBUG
        printf("Done!\n");
        printf("Converting input parameters into a config struct...\n");
#endif
        conf.first_name = argv[2];
        conf.last_name = argv[3];
        conf.email = argv[4];

#ifdef DEBUG
        printf("Done!\n");
#endif
        writeConfig(conf);
    }
    else if (strcmp(parameter, "write") == 0)
    {
        readConfig(&conf);
#ifdef DEBUG
        printf("Checking for sufficent number of arguments.\n");
#endif
        if (argc < 3)
        {
            fprintf(stderr, "Insufficent number of arguments provided.\n");
            help();
            exit(1);
        }
#ifdef DEBUG
        printf("Done!\n");
#endif
        writeLicense(conf, argv[2]);
    }
    else if (strcmp(parameter, "custom") == 0)
    {
#ifdef DEBUG
        printf("Checking if sufficent number of arguments have been provided.\n");
#endif
        if (argc < 6)
        {
            fprintf(stderr, "Insufficent number of arguments provided!\n");
            help();
            exit(1);
        }
#ifdef DEBUG
        printf("Converting input to config struct...\n");
#endif
        conf.first_name = argv[3];
        conf.last_name = argv[4];
        conf.email = argv[5];
#ifdef DEBUG
        printf("FIRST_NAME: %s\nLAST_NAME: %s\nEMAIL: %s\n", conf.first_name, conf.last_name, conf.email);
#endif
        writeLicense(conf, argv[2]);
    }
    else
    {
        fprintf(stderr, "Provided parameter not found!\n");
        help();
        exit(1);
    }

    (void)conf;
    (void)argc;
    (void)argv;
    return 0;
}
