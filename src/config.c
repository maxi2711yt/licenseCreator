#include "../include/config.h"
#include "../include/defines.h"

char *strsub(const char *str, size_t a, size_t b)
{
#ifdef DEBUG
    printf("Checking if a or b are greater than the input string...\n");
#endif
    if (a > strlen(str) || b > strlen(str))
    {
#ifdef DEBUG
        printf("a or b are greater than the inputs string length!\n");
#endif
        return NULL;
    }

#ifdef DEBUG
    printf("Checking if a is greater than b...\n");
#endif
    if (a > b)
    {
#ifdef DEBUG
        printf("a is greater than b returning.\n");
#endif
        return NULL;
    }

#ifdef DEBUG
    printf("Computing size of the output string...\n");
#endif
    int size = a - b;
    if (size < 0)
        size = size * -1;
#ifdef DEBUG
    printf("Allocating string of size %d...\n", size + 1);
#endif
    char *out = malloc(size + 1);
    out[size] = '\0';
#ifdef DEBUG
    printf("Done!\n");
#endif

#ifdef DEBUG
    printf("Done!\n");
    printf("Creating substring in out...\n");
#endif
    for (size_t i = a, o = 0; i < b; i++, o++)
    {
        out[o] = str[i];
    }
#ifdef DEBUG
    printf("Done!\n");
#endif
    return out;
}

void readConfig(config *conf)
{
    FILE *f;
#ifdef DEBUG
    printf("Opening file...\n");
#endif
    char *path = getenv("HOME");
    strcat(path, CONFIG_PATH);
#ifdef DEBUG
    printf("Checking if directory \"%s\" exists...\n", path);
#endif
    struct stat st = {0};
    if (stat(path, &st) == -1)
    {
#ifdef DEBUG
        printf("Config dirertory doesn't exist.\n");
#endif
        fprintf(stderr, "Config directory doesn't exist. Make sure to run\nlicenseCreator init [FirstName] [LastName] "
                        "[Email]\nto initialize the config file.\n");
        exit(1);
    }
#ifdef DEBUG
    else
        printf("Directory exists!\n");
#endif
    strcat(path, CONFIG_FILE_NAME);
#ifdef DEBUG
    printf("Config file path is: %s\n", path);
#endif
    f = fopen(path, "r");
    if (f == NULL)
    {
        fprintf(stderr, "ERROR: Config file couldn't be opened.\n");
        exit(1);
    }
#ifdef DEBUG
    printf("File has been opened...\n");
    printf("Calculating file size...\n");
#endif
    size_t filesize;
    fseek(f, 0, SEEK_END);
    filesize = ftell(f);
    rewind(f);
#ifdef DEBUG
    printf("Done!\n");
    printf("Allocating outputBuffer and reading from config file...\n");
#endif
    char outputBuffer[filesize + 1];
    fread(outputBuffer, sizeof(char), filesize, f);
    outputBuffer[filesize] = '\0';

#ifdef DEBUG
    printf("Done!\n");
    printf("Reading first_name...\n");
#endif
    size_t start = 0;
    for (size_t i = start; i < filesize; i++)
    {
        if (outputBuffer[i] == '\n')
        {
            conf->first_name = strsub(outputBuffer, start, i);
            start = i + 1;
            break;
        }
    }
#ifdef DEBUG
    printf("Done!\n");
    printf("Reading last_name...\n");
#endif
    for (size_t i = start; i < filesize; i++)
    {
        if (outputBuffer[i] == '\n')
        {
            conf->last_name = strsub(outputBuffer, start, i);
            start = i + 1;
            break;
        }
    }
#ifdef DEBUG
    printf("Done!\n");
    printf("Reading email...\n");
#endif
    for (size_t i = start; i < filesize; i++)
    {
        if (outputBuffer[i] == '\n')
        {
            conf->email = strsub(outputBuffer, start, i);
            break;
        }
    }
#ifdef DEBUG
    printf("Read:\nFirst Name = \"%s\"\nLast Name = \"%s\"\nEmail = \"%s\"\n", conf->first_name, conf->last_name,
           conf->email);
#endif
    // Closing the file
    fclose(f);
}

void writeConfig(const config conf)
{
    // Creating and opening file
    FILE *f;
#ifdef DEBUG
    printf("Opening file...\n");
#endif
    char *path = getenv("HOME");
    strcat(path, CONFIG_PATH);
#ifdef DEBUG
    printf("Checking if directory \"%s\" exists...\n", path);
#endif
    struct stat st = {0};
    if (stat(path, &st) == -1)
    {
#ifdef DEBUG
        printf("Creating directory: %s\n", path);
#endif
        mkdir(path, 0700);
    }
#ifdef DEBUG
    else
        printf("Directory exists!\n");
#endif
    strcat(path, CONFIG_FILE_NAME);
#ifdef DEBUG
    printf("Config file path is: %s\n", path);
#endif
    f = fopen(path, "w");
    if (f == NULL)
    {
        fprintf(stderr, "ERROR: Config file couldn't be opened.\n");
        exit(1);
    }
#ifdef DEBUG
    printf("File has been opened...\n");
    printf("Writing to file...\n");
#endif
    // Writing to the config file
    fprintf(f, "%s\n%s\n%s\n", conf.first_name, conf.last_name, conf.email);

#ifdef DEBUG
    printf("Done!\n");
#endif
    // Closing the file
    fclose(f);
}
